import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-content-left',
  templateUrl: './content-left.component.html',
  styleUrls: ['./content-left.component.css']
})
export class ContentLeftComponent implements OnInit {

  constructor() { }
  templates = [{
    name: 'Strength',
    value: 'strength',
    modifierValue: 'strengthmodifier'
  },
  {
    name: 'Dexterity',
    value: 'dexterity',
    modifierValue: 'dexterityhmodifier'
  },
  {
    name: 'Constitution',
    value: 'constitution',
    modifierValue: 'constitutionmodifier'
  },
  {
    name: 'Intelligence',
    value: 'intelligence',
    modifierValue: 'intelligencemodifier'
  },
  {
    name: 'Wisdom',
    value: 'wisdom',
    modifierValue: 'wisdommodifier'
  }, {
    name: 'Charisma',
    value: 'charisma',
    modifierValue: 'charismamodifier'
  }
];

ngOnInit() {
  }
}
