import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ContentTopComponent } from './content-top/content-top.component';
import { ContentLeftComponent } from './content-left/content-left.component';
import { AbilityTemplateComponent } from './ability-template/ability-template.component';

@NgModule({
   declarations: [
      AppComponent,
      ContentTopComponent,
      AbilityTemplateComponent,
      ContentLeftComponent
   ],
   imports: [
      BrowserModule
      ],
   providers: [],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }
