import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-ability-template',
  templateUrl: './ability-template.component.html',
  styleUrls: ['./ability-template.component.css']
})

export class AbilityTemplateComponent implements OnInit {
  @Input()
  name: string;

  @Input()
  modifierValue: string;

  @Input()
  value: string;

  constructor() { }

  ngOnInit() {
  }

}
